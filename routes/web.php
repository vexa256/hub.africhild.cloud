<?php

use App\Http\Controllers\UpdateLogic;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ResourceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::controller(MainController::class)->group(function () {
    Route::any('ViewResourceFiles/{id}', 'ViewResourceFiles')->name('ViewResourceFiles');
    Route::any('GetTopics/{id}', 'GetTopics')->name('GetTopics');
    Route::any('GetSubTopics/{id}', 'GetSubTopics')->name('GetSubTopics');
    Route::any('ViewThemes', 'ViewThemes')->name('ViewThemes');

});

Route::controller(ResourceController::class)->group(function () {

    Route::get('ExploreRes', 'ExploreRes')->name('ExploreRes');
    Route::get('ViewCategories/{id}', 'ViewCategories')->name('ViewCategories');
    Route::get('/', 'home');

    Route::get('home', 'home')->name('home');

    Route::get('dashboard', 'home')->name('dashboard');

});

Route::middleware(['auth'])->group(function () {

    /****
     * Resources Routes
     */

    Route::controller(MainController::class)->group(function () {

        Route::any('NewResource', 'NewResource')->name('NewResource');
        Route::get('MgtSubTopics', 'MgtSubTopics')->name('MgtSubTopics');
        Route::get('MgtMainResources', 'MgtMainResources')->name('MgtMainResources');
        Route::get('MgtTopics', 'MgtTopics')->name('MgtTopics');
        Route::get('MgtThemes', 'MgtThemes')->name('MgtThemes');
    });

    Route::controller(ResourceController::class)->group(function () {

        Route::get('MgtUsers', 'MgtUsers')->name('MgtUsers');

        Route::get('AdminSettings', 'AdminSettings')->name('AdminSettings');

        Route::get('MgtCategories', 'MgtCategories')->name('MgtCategories');

        Route::get('MgtResources', 'MgtResources')->name('MgtResources');
    });
    /**Trainings Routes
     * ***
     */
    //

    Route::controller(UpdateLogic::class)->group(function () {

        Route::get('/MassDelete/{id}/{TableName}', 'MassDelete')->name('MassDelete');

        Route::post('/SubTopicsFix', 'SubTopicsFix')->name('SubTopicsFix');

        Route::post('/AutoInsert', 'AutoInsert')->name('AutoInsert');

        Route::post('/AutoUpdate', 'AutoUpdate')->name('AutoUpdate');

    });

});

require __DIR__ . '/auth.php';
