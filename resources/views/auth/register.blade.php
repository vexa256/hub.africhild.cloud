@include('header.header')


<div class="d-flex flex-column flex-root">
    <!--begin::Authentication - Sign-in -->
    <div class="d-flex flex-column flex-lg-row flex-column-fluid">
        <!--begin::Aside-->
        <div class="d-flex flex-column flex-lg-row-auto w-xl-600px positon-xl-relative"
            style="background-image: url({{ asset('cover.png') }})">
            <!--begin::Wrapper-->
            <div class="d-flex flex-column position-xl-fixed top-0 bottom-0 w-xl-600px ">
                <!--begin::Content-->
                <div style="background-color:rgba(0, 0, 0, 0.5)"
                    class="d-flex flex-row-fluid flex-column text-center p-10 pt-lg-20">
                    <!--begin::Logo-->

                    <!--end::Logo-->
                    <!--begin::Title-->
                    <h1 class="fw-bolder mt-20 fs-2qx pb-5 pb-md-10 text-light">AfriChild Knowledge Hub </h1>
                    <!--end::Title-->
                    <!--begin::Description-->
                    <p class="fw-bold fs-2 text-light">

                    </p>
                    <!--end::Description-->
                    <a href="{{ asset('cover.png') }}" class="py-9 mt-20 pt-20">
                        <img alt="Logo" src="{{ asset('logo.png') }}" class="h-70px" />
                    </a>

                </div>

                <!--end::Content-->
                <!--begin::Illustration-->

                <!--end::Illustration-->
            </div>
            <!--end::Wrapper-->
        </div>


        <!--end::Aside-->
        <!--begin::Body-->
        <div class="d-flex flex-column flex-lg-row-fluid py-10">
            <!--begin::Content-->
            <div class="d-flex flex-center flex-column flex-column-fluid">
                <!--begin::Wrapper-->
                <div class="w-lg-500px p-10 p-lg-15 mx-auto">
                    <!--begin::Form-->

                    <!--begin::Heading-->
                    <div class="text-center mb-10">
                        <!--begin::Title-->
                        <h1 class="text-dark mb-3">Knowledge Hub Sign Up </h1>
                        <!--end::Title-->
                        <!--begin::Link-->
                        <div class="text-gray-400 fw-bold fs-4">I have an account
                            <a href="{{ url('login') }}" class="link-danger fw-bolder">Login</a>
                        </div>
                        <!--end::Link-->
                    </div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="fv-row mb-10">
                            <!--begin::Wrapper-->
                            <div class="d-flex flex-stack mb-2">
                                <!--begin::Label-->
                                <label class="form-label fw-bolder text-dark fs-6 mb-0">Name</label>
                                <!--end::Label-->
                                <!--begin::Link-->

                                <!--end::Link-->
                            </div>
                            <!--end::Wrapper-->
                            <!--begin::Input-->
                            <input id="name" class="block form-control mt-1 w-full" type="text" name="name"
                                :value="old('name')" required autofocus />
                            <!--end::Input-->
                        </div>
                        <!--begin::Heading-->
                        <!--begin::Input group-->
                        <div class="fv-row mb-10">
                            <!--begin::Label-->
                            <label class="form-label fs-6 fw-bolder text-dark">Email</label>
                            <!--end::Label-->
                            <!--begin::Input-->
                            <input id="email" class="block form-control mt-1 w-full" type="email" name="email"
                                :value="old('email')" required autofocus />
                            <!--end::Input-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->


                        <div class="fv-row mb-10">
                            <!--begin::Wrapper-->
                            <div class="d-flex flex-stack mb-2">
                                <!--begin::Label-->
                                <label class="form-label fw-bolder text-dark fs-6 mb-0">Password</label>
                                <!--end::Label-->
                                <!--begin::Link-->

                                <!--end::Link-->
                            </div>
                            <!--end::Wrapper-->
                            <!--begin::Input-->
                            <input id="password" class="block form-control mt-1 w-full" type="password" name="password"
                                required autocomplete="current-password" />
                            <!--end::Input-->
                        </div>

                        <div class="fv-row mb-10">
                            <!--begin::Wrapper-->
                            <div class="d-flex flex-stack mb-2">
                                <!--begin::Label-->
                                <label class="form-label fw-bolder text-dark fs-6 mb-0">Confirm Password</label>
                                <!--end::Label-->
                                <!--begin::Link-->

                                <!--end::Link-->
                            </div>
                            <!--end::Wrapper-->
                            <!--begin::Input-->
                            <input id="password_confirmation" class="block form-control mt-1 w-full" type="password"
                                name="password_confirmation" required />
                            <!--end::Input-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Actions-->
                        <div class="text-center">
                            <!--begin::Submit button-->
                            <button type="submit" class="btn btn-lg btn-danger w-100 mb-5">
                                Create Account

                            </button>

                        </div>
                        <!--end::Actions-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Wrapper-->
            </div>
            <!--end::Content-->
            <!--begin::Footer-->
            <div class="d-flex flex-center flex-wrap fs-6 p-5 pb-0">
                <!--begin::Links-->
                <div class="d-flex flex-center fw-bold fs-6">
                    <a href="https://africhild.or.ug/Home" class="text-muted text-hover-danger px-2">AfriChild
                        Home</a>
                    <a href="/register" class="text-muted text-hover-danger px-2">Register</a>
                    <a href="/login" class="text-muted text-hover-danger px-2">Login</a>
                </div>
                <!--end::Links-->
            </div>
            <!--end::Footer-->
        </div>
        <!--end::Body-->


    </div>
    <!--end::Authentication - Sign-in-->
</div>


@include('scripts.scripts')
