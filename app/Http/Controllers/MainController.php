<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\FormEngine;

class MainController extends Controller
{

    public function MgtThemes()
    {

        $Themes     = DB::table('themes')->get();
        $FormEngine = new FormEngine;

        $rem = ['id', 'created_at', 'updated_at', 'ThemeID', 'TopicID', 'SubTopicID', 'uuid', 'CID', 'Code', 'status', 'UUID', 'Thumbnail',
        ];
        $data = [
            'Title'  => 'Manage the AfriChild Center Knowledge Hub Themes',
            'Desc'   => 'Theme Settings',
            'rem'    => $rem,
            'Themes' => $Themes,
            'Form'   => $FormEngine->Form('themes'),
            'Page'   => 'Themes.MgtThemes',

        ];

        return view('scrn', $data);
    }

    public function MgtTopics()
    {
        $Topics = DB::table('topics AS T')
            ->join('themes AS H', 'H.ThemeID', 'T.ThemeID')
            ->select('T.*', 'H.Title AS Theme', 'H.ThemeID')->get();
        $Themes     = DB::table('themes')->get();
        $FormEngine = new FormEngine;

        $rem = ['id', 'created_at', 'updated_at', 'ThemeID', 'TopicID', 'SubTopicID', 'uuid', 'CID', 'Code', 'status', 'UUID', 'Thumbnail',
        ];
        $data = [
            'Title'  => 'Manage the AfriChild Center Knowledge Hub Theme Topics',
            'Desc'   => 'Theme Topic Settings',
            'rem'    => $rem,
            'Topics' => $Topics,
            'Themes' => $Themes,
            'Form'   => $FormEngine->Form('topics'),
            'Page'   => 'Topics.MgtTopics',

        ];

        return view('scrn', $data);
    }

    public function MgtSubTopics()
    {
        $SubTopics = DB::table('sub_topics AS T')
            ->join('topics AS H', 'H.TopicID', 'T.TopicID')
            ->join('themes AS TH', 'TH.ThemeID', 'T.ThemeID')
            ->select('T.*', 'H.Title AS Topic', 'H.TopicID')
            ->get();
        // dd($SubTopics);
        // dd($SubTopics);

        $Topics = DB::table('topics AS T')
            ->join('themes AS TH', 'TH.ThemeID', 'T.ThemeID')
            ->select('T.*')
            ->get();

        // dd($Topics->count());

        $FormEngine = new FormEngine;

        $rem = ['id', 'created_at', 'updated_at', 'ThemeID', 'TopicID', 'SubTopicID', 'uuid', 'CID', 'Code', 'status', 'UUID', 'Thumbnail',
        ];
        $data = [
            'Title'     => 'Manage the AfriChild Center Knowledge Hub Theme Topics',
            'Desc'      => 'Theme Topic Settings',
            'rem'       => $rem,
            'Topics'    => $Topics,
            'SubTopics' => $SubTopics,
            'Form'      => $FormEngine->Form('sub_topics'),
            'Page'      => 'SubTopics.MgtSubTopics',

        ];

        return view('scrn', $data);
    }

    public function MgtMainResources()
    {

        $Topics = DB::table('topics AS T')
            ->join('themes AS TH', 'TH.ThemeID', 'T.ThemeID')
            ->select('T.*')
            ->get();

        $Themes    = DB::table('themes')->get();
        $SubTopics = DB::table('sub_topics')->get();

        $Resources = DB::table('resources AS R')
            ->join('themes AS H', 'H.ThemeID', 'R.ThemeID')
            ->join('topics AS T', 'T.TopicID', 'R.TopicID')
            ->join('sub_topics AS S', 'S.SubTopicID', 'R.SubTopicID')
            ->select('R.*', 'H.Title AS Theme', 'T.Title AS Topic', 'S.Title AS SubTopic', 'H.ThemeID', 'T.TopicID', 'S.SubTopicID')
            ->get();

        $FormEngine = new FormEngine;

        $rem = ['id', 'AttachmentFile', 'created_at', 'updated_at', 'ThemeID', 'TopicID', 'SubTopicID', 'uuid', 'CID', 'Code', 'status', 'UUID', 'Thumbnail',
        ];
        $data = [
            'Title'     => 'Manage Resource Documentation',
            'Desc'      => 'Resource Management',
            'rem'       => $rem,
            'Themes'    => $Themes,
            'Topics'    => $Topics,
            'SubTopics' => $SubTopics,
            'Resources' => $Resources,
            'Form'      => $FormEngine->Form('resources'),
            'Page'      => 'Resources.Resources',

        ];

        return view('scrn', $data);
    }

    public function NewResource(Request $request)
    {

        $request->validate([
            '*'              => 'required',
            'Thumbnail'      => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4040',
            'AttachmentFile' => 'required|file|max:50048',

        ]);

        DB::table($request->TableName)->insert(
            $request->except([
                '_token',
                'TableName',
                'id',
            ])
        );

        $this->UploadFile(

            $request->Thumbnail, $request->TableName, 'Thumbnail', $request->uuid

        );

        $this->UploadFile(

            $request->AttachmentFile, $request->TableName, 'AttachmentFile', $request->uuid

        );

        return redirect()->back()->with('status', 'Record created successfully');

    }

    public function UploadFile($File, $TableName, $Col, $uuid)
    {
        $FileName = time() . '.' . $File->extension();

        $File->move(public_path('assets/docs'), $FileName);

        DB::table($TableName)->where('uuid', $uuid)->update([

            $Col => 'assets/docs/' . $FileName,
        ]);
    }

    public function ViewThemes()
    {

        $Themes = DB::table('themes')->get();
        $data   = [
            'Title'  => 'Select Knowledge Hub Theme To Explore',
            'Desc'   => 'Themes',
            'Themes' => $Themes,
            'Page'   => 'visitor.ViewThemes',

        ];

        return view('scrn', $data);
    }

    public function GetTopics($id)
    {
        $Themes = DB::table('themes')->where('id', $id)->first();

        $Topics = DB::table('topics')->where('ThemeID', $Themes->ThemeID)->get();

        $data = [
            'Title'  => 'Explore topics under the Knowledge hub theme ' . $Themes->Title,
            'Desc'   => 'Topics under the selected theme',
            'Topics' => $Topics,
            'Page'   => 'visitor.ViewTopics',

        ];

        return view('scrn', $data);
    }

    public function GetSubTopics($id)
    {
        $Topics = DB::table('topics')->where('id', $id)->first();

        // dd($Topics = DB::table('topics')->where('id', $id)->first());

        $SubTopics = DB::table('sub_topics')->where('TopicID', $Topics->TopicID)->get();

        // dd($SubTopics);

        $data = [
            'Title'     => 'Explore sub-topics under the Knowledge hub topic ' . $Topics->Title,
            'Desc'      => 'Sub-topics under the selected parent topic',
            'SubTopics' => $SubTopics,
            'Page'      => 'visitor.ViewSubTopics',

        ];

        return view('scrn', $data);
    }

    public function ViewResourceFiles($id)
    {

        $SubTopics = DB::table('sub_topics')->where('id', $id)->first();
        $Resources = DB::table('resources')
            ->where('SubTopicID', $SubTopics->SubTopicID)->get();

        $data = [
            'Title'     => 'View knowledge hub resources under the sub-topic ' . $SubTopics->Title,
            'Desc'      => 'Resources under the selected sub topic',
            'SubTopics' => $SubTopics,
            'Resources' => $Resources,
            'Page'      => 'visitor.Res',

        ];

        return view('scrn', $data);
    }
}
