<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FormEngine;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class TrainingController extends Controller
{

    /**
     * Class constructor.
     */
    public function __construct()
    {

        $counter = DB::table('trainings')
            ->whereDate('EndDate', '<', Carbon::now()->format('Y-m-d'))
            ->where('status', 'true')
            ->count();

        if ($counter > 0) {

            $Update = DB::table('trainings')
                ->whereDate('EndDate', '<', Carbon::now()->format('Y-m-d'))
                ->where('status', 'true')
                ->get();

            foreach ($Update as $data) {

                $up = DB::table('trainings')->where('id', $data->id)->update([
                    'status' => 'false',
                ]);

            }

        }

    }
    public function MgtTrainings()
    {

        $Trainings = DB::table('trainings')->get();

        $FormEngine = new FormEngine;

        $rem = [
            'id',
            'created_at',
            'updated_at',
            'TID',
            'Code',
            'status',
            'UUID',
            'Thumbnail',

        ];

        $data = [
            'Title'     => 'Manage Training Sessions ',
            'Desc'      => 'Create , Update  and Edit Training Sessions ',
            'Page'      => 'Trainings.MgtTrainings',
            'Form'      => $FormEngine->Form('trainings'),
            'rem'       => $rem,
            'Trainings' => $Trainings,

        ];

        return view('scrn', $data);
    }

    public function SelectVideo()
    {

        $Trainings = DB::table('trainings')->where('status', 'true')->get();
        $data      = [
            'Title'     => 'Attach videos to a training session',
            'Desc'      => 'Select training session to attach video materials to',
            'Page'      => 'Materials.SelectVideo',
            //'Form'      => $FormEngine->Form('trainings'),
            //'rem'       => $rem,
            'Trainings' => $Trainings,

        ];

        return view('scrn', $data);
    }

    public function VideoSelected(Request $request)
    {
        $request->validate([
            '*' => 'required',
        ]);

        return redirect()->route('AttachVideo', ['id' => $request->id]);
    }

    public function AttachVideo($id)
    {

        $FormEngine = new FormEngine;

        $rem = [
            'id',
            'created_at',
            'updated_at',
            'TID',
            'AttachmentFile',
            'UUID',
            'Thumbnail',

        ];
        $T = DB::table('trainings')->where('id', $id)->first();

        $Videos = DB::table('training_videos')
            ->where('TID', $T->TID)
            ->get();

        $data = [
            'Title'  => 'Attach videos to the  training session ',
            'Desc'   => 'Selected training | ' . $T->TrainingTitle,
            'Page'   => 'Materials.AttachVideo',
            'Form'   => $FormEngine->Form('training_videos'),
            'rem'    => $rem,
            'T'      => $T,
            'Videos' => $Videos,

        ];

        return view('scrn', $data);
    }

    public function SelectNotes()
    {

        $Trainings = DB::table('trainings')->where('status', 'true')->get();
        $data      = [
            'Title'     => 'Attach text notes to a training session',
            'Desc'      => 'Select training session to attach text materials to',
            'Page'      => 'Materials.SelectNotes',
            //'Form'      => $FormEngine->Form('trainings'),
            //'rem'       => $rem,
            'Trainings' => $Trainings,

        ];

        return view('scrn', $data);

    }

    public function NotesSelected(Request $request)
    {
        $request->validate([
            '*' => 'required',
        ]);

        return redirect()->route('AttachNotes', ['id' => $request->id]);
    }

    public function AttachNotes($id)
    {

        $FormEngine = new FormEngine;

        $rem = [
            'id',
            'created_at',
            'updated_at',
            'TID',
            'AttachmentFile',
            'UUID',
            'Thumbnail',

        ];
        $T = DB::table('trainings')->where('id', $id)->first();

        $Notes = DB::table('training_notes')
            ->where('TID', $T->TID)
            ->get();

        $data = [
            'Title' => 'Attach text notes to the  training session ',
            'Desc'  => 'The selected training  is | ' . $T->TrainingTitle,
            'Page'  => 'Materials.AttachNotes',
            'Form'  => $FormEngine->Form('training_notes'),
            'rem'   => $rem,
            'T'     => $T,
            'Notes' => $Notes,

        ];

        return view('scrn', $data);
    }

    public function SelectDocs()
    {

        $Trainings = DB::table('trainings')->where('status', 'true')->get();
        $data      = [
            'Title'     => 'Attach PDF documents to a training session',
            'Desc'      => 'Select training session to attach pdf materials to',
            'Page'      => 'Materials.SelectDocs',
            //'Form'      => $FormEngine->Form('trainings'),
            //'rem'       => $rem,
            'Trainings' => $Trainings,

        ];

        return view('scrn', $data);
    }

    public function DocsSelected(Request $request)
    {
        $request->validate([
            '*' => 'required',
        ]);

        return redirect()->route('AttachDocs', ['id' => $request->id]);
    }

    public function AttachDocs($id)
    {
        $FormEngine = new FormEngine;

        $rem = [
            'id',
            'created_at',
            'updated_at',
            'TID',
            'AttachmentFile',
            'UUID',
            'Thumbnail',

        ];
        $T = DB::table('trainings')->where('id', $id)->first();

        $Docs = DB::table('training_docs')
            ->where('TID', $T->TID)
            ->get();

        $data = [
            'Title' => 'Attach PDF documents to the selected  training session ',
            'Desc'  => 'The selected training  is | ' . $T->TrainingTitle,
            'Page'  => 'Materials.AttachDocs',
            'Form'  => $FormEngine->Form('training_docs'),
            'rem'   => $rem,
            'T'     => $T,
            'Docs'  => $Docs,

        ];

        return view('scrn', $data);
    }
    public function SelectTrainingQuiz()
    {
        $Trainings = DB::table('trainings')->where('status', 'true')->get();
        $data      = [
            'Title'     => 'Add a quiz to the selected training',
            'Desc'      => 'This quiz will be attempted by the trainees',
            'Page'      => 'Quiz.SelectTraining',
            //'Form'      => $FormEngine->Form('trainings'),
            //'rem'       => $rem,
            'Trainings' => $Trainings,

        ];

        return view('scrn', $data);
    }

    public function QuizSelected(Request $request)
    {
        $request->validate([
            '*' => 'required',
        ]);

        return redirect()->route('MgtQuiz', ['id' => $request->id]);
    }

    public function MgtQuiz($id)
    {
        $FormEngine = new FormEngine;

        $rem = [
            'id',
            'created_at',
            'updated_at',
            'QID',
            'TID',
            'AttachmentFile',
            'UUID',
            'Thumbnail',

        ];
        $T = DB::table('trainings')->where('id', $id)->first();

        $Quiz = DB::table('quizzes')
            ->where('TID', $T->TID)
            ->get();

        $data = [
            'Title' => 'Add quiz  to the selected  training session ',
            'Desc'  => 'The selected training  is | ' . $T->TrainingTitle,
            'Page'  => 'Quiz.MgtQuiz',
            'Form'  => $FormEngine->Form('quizzes'),
            'rem'   => $rem,
            'T'     => $T,
            'Quiz'  => $Quiz,

        ];

        return view('scrn', $data);
    }

    public function SelectQuiz()
    {
        $Quiz = DB::table('quizzes')->get();
        $data = [
            'Title' => 'Add a question bank to a quiz',
            'Desc'  => 'Select quiz to attach a question bank to',
            'Page'  => 'Quiz.SelectQtn',
            //'Form'      => $FormEngine->Form('trainings'),
            //'rem'       => $rem,
            'Quiz'  => $Quiz,

        ];

        return view('scrn', $data);
    }

    public function QuizExamSelected(Request $request)
    {
        $request->validate([
            '*' => 'required',
        ]);

        return redirect()->route('SetQuestions', ['id' => $request->id]);
    }

    public function SetQuestions($id)
    {
        $FormEngine = new FormEngine;

        $rem = [
            'id',
            'created_at',
            'updated_at',
            'QID',
            'TID',
            'AttachmentFile',
            'UUID',
            'Thumbnail',
            'UUID',
            'QTNID',

        ];
        $T = DB::table('quizzes')->where('id', $id)->first();

        $Tran = DB::table('trainings')->where('TID', $T->TID)->first();

        $Exams = DB::table('quiz_questions AS QE')

            ->where('QE.TID', $T->TID)

            ->get();

        $data = [
            'Title' => 'Add questions to the selected quiz',
            'Desc'  => 'The selected quiz  is ' . $T->QuizTitle . ' and the training is ' . $Tran->TrainingTitle,
            'Page'  => 'Quiz.Question',
            'Form'  => $FormEngine->Form('quiz_questions'),
            'rem'   => $rem,
            'T'     => $T,
            'Exams' => $Exams,
            'Tran'  => $Tran,

        ];

        return view('scrn', $data);
    }

    public function SelectFeed()
    {
        $Trainings = DB::table('trainings')->get();
        $data      = [
            'Title'     => 'Select training whose feedback collection is required',
            'Desc'      => 'Select training to attach feedback report to',
            'Page'      => 'Quiz.SelectFeed',
            //'Form'      => $FormEngine->Form('trainings'),
            //'rem'       => $rem,
            'Trainings' => $Trainings,

        ];

        return view('scrn', $data);
    }

    public function FeedSelected(Request $request)
    {
        $request->validate([
            '*' => 'required',
        ]);

        return redirect()->route('ReadFeedback', ['id' => $request->id]);
    }

    public function ReadFeedback($id)
    {

        $T = DB::table('trainings')->where('id', $id)->first();

        $Feed = DB::table('quiz_feebacks')
            ->where('TID', $T->TID)
            ->get();

        $data = [
            'Title' => 'View all the feedback provided by the trainees about this training',
            'Desc'  => 'The selected training  is ' . $T->TrainingTitle,
            'Page'  => 'Quiz.Feedback',
            'T'     => $T,
            'Feed'  => $Feed,

        ];

        return view('scrn', $data);
    }

    public function ReportTraining()
    {

        $Trainings = DB::table('trainings')->get();
        $data      = [
            'Title'     => 'Create training report',
            'Desc'      => 'Select training to attach the  report to',
            'Page'      => 'Reports.SelectTraining',
            //'Form'      => $FormEngine->Form('trainings'),
            //'rem'       => $rem,
            'Trainings' => $Trainings,

        ];

        return view('scrn', $data);
    }

    public function ReportSelected(Request $request)
    {
        $request->validate([
            '*' => 'required',
        ]);

        return redirect()->route('MgtReport', ['id' => $request->id]);
    }

    public function MgtReport($id)
    {
        $FormEngine = new FormEngine;

        $rem = [
            'id',
            'created_at',
            'updated_at',
            'TID',
            'AttachmentFile',
            'ReportAuthor',
            'UUID',
            'Thumbnail',

        ];
        $T = DB::table('trainings')->where('id', $id)->first();

        $Reports = DB::table('training_reports')
            ->where('TID', $T->TID)
            ->get();

        $data = [
            'Title'   => 'Attach videos to the  training session ',
            'Desc'    => 'Selected training | ' . $T->TrainingTitle,
            'Page'    => 'Reports.TrainingReport',
            'Form'    => $FormEngine->Form('training_reports'),
            'rem'     => $rem,
            'T'       => $T,
            'Reports' => $Reports,

        ];

        return view('scrn', $data);
    }

    public function ViewTrainings()
    {
        $Trainings = DB::table('trainings')->get();

        $data = [
            'Title' => 'Select training session to attend',
            'Desc'  => 'Only ongoing trainings are accessible',
            'Page'  => 'Trainings.ViewTrain',

            'Res'   => $Trainings,

        ];

        return view('scrn', $data);
    }

    public function AttendTraining($id)
    {
        $T      = DB::table('trainings')->where('id', $id)->first();
        $Videos = DB::table('training_videos')->where('TID', $T->TID)->get();
        $Docs   = DB::table('training_docs')->where('TID', $T->TID)->get();
        $Text   = DB::table('training_notes')->where('TID', $T->TID)->get();

        $data = [
            'Title'  => 'View resources attached to the training ' . $T->TrainingTitle,
            'Desc'   => 'Select the desired resource to view it',
            'Page'   => 'Trainings.Attend',

            'T'      => $T,
            'Videos' => $Videos,
            'Docs'   => $Docs,
            'Text'   => $Text,

        ];

        return view('scrn', $data);
    }
}
