<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FormEngine;
use DB;

class ResourceController extends Controller
{

    public function home()
    {
        $data = [
            'Title' => 'Welcome to the AfriChild Knowledge Hub ',
            'Desc'  => 'Explore resources and training materials ',
            'Page'  => 'Home.Home',

        ];

        return view('scrn', $data);
    }
    public function MgtCategories()
    {
        $Cat = DB::table('resource_categories')->get();

        $FormEngine = new FormEngine;

        $rem = [
            'id',
            'created_at',
            'updated_at',
            'TID',
            'CID',
            'Code',
            'status',
            'UUID',
            'Thumbnail',

        ];

        $data = [
            'Title' => 'Manage resource categories',
            'Desc'  => 'Create , Update  and Edit Resources Categories ',
            'Page'  => 'Resources.ResCats',
            'Form'  => $FormEngine->Form('resource_categories'),
            'rem'   => $rem,
            'Cat'   => $Cat,

        ];

        return view('scrn', $data);
    }

    public function MgtResources()
    {
        $Cat = DB::table('resource_categories')->get();

        $Res = DB::table('resources AS R')
            ->join('resource_categories AS RC', 'RC.CID', '=', 'R.CID')
            ->select('RC.CategoryTitle', 'R.*')
            ->get();

        // dd($Res);

        $FormEngine = new FormEngine;

        $rem = [
            'id',
            'created_at',
            'updated_at',
            'TID',
            'CID',
            'Code',
            'status',
            'UUID',
            'Thumbnail',
            'AttachmentFile',

        ];

        $data = [
            'Title' => 'Manage all publically accessible resources',
            'Desc'  => 'Create , Update  and Edit Resources  ',
            'Page'  => 'Resources.Resources',
            'Form'  => $FormEngine->Form('resources'),
            'rem'   => $rem,
            'Cat'   => $Cat,
            'Res'   => $Res,

        ];

        return view('scrn', $data);
    }

    public function ExploreRes()
    {
        $Cat = DB::table('resource_categories')->get();

        $data = [
            'Title' => 'Our public resource categories',
            'Desc'  => 'Select the resource category to explore',
            'Page'  => 'Resources.UserResCats',

            'Cats'  => $Cat,

        ];

        return view('scrn', $data);
    }

    public function ViewCategories($id)
    {
        $Cat = DB::table('resource_categories')->where('id', $id)->first();
        $Res = DB::table('resources')->where('CID', $Cat->CID)->get();

        $Counter = DB::table('resources')->where('CID', $Cat->CID)
            ->count();

        if ($Counter < 1) {

            return redirect()->back()->with('error_a', 'We are currently populating the selected catergory, Please select another one');
        }

        $data = [
            'Title' => 'View public resources under the category ' . $Cat->CategoryTitle,
            'Desc'  => 'Click on the resource card to open it',
            'Page'  => 'Resources.ViewRes',
            'Cats'  => $Cat,
            'Res'   => $Res,

        ];

        return view('scrn', $data);
    }

    public function AdminSettings()
    {
        $Admins = DB::table('users')->where('Role', 'Admin')
            ->orWhere('Role', 'SuperAdmin')
            ->get();

        $FormEngine = new FormEngine;

        $rem = [
            'id',
            'created_at',
            'updated_at',
            'TID',
            'CID',
            'UID',
            'Phone',
            'Proffession',
            'Nationality',
            'Sex',
            'Phone',
            'remember_token',
            'email_verified_at',
            'Role',

            'status',
            'UUID',
            'Thumbnail',
            'AttachmentFile',

        ];

        $data = [
            'Title' => 'Manage all accounts that administer this system',
            'Desc'  => 'Admin previllage management ',
            'Page'  => 'Users.Admin',
            'Form'  => $FormEngine->Form('users'),
            'rem'   => $rem,
            'Admin' => $Admins,

        ];

        return view('scrn', $data);
    }

    public function MgtUsers()
    {
        $Admins = DB::table('users')->whereNot('Role', 'Admin')
            ->whereNot('Role', 'SuperAdmin')
            ->get();

        $data = [
            'Title' => 'Manage all users accounts ',
            'Desc'  => 'Explore all users accessing the  system',
            'Page'  => 'Users.Users',

            'Admin' => $Admins,

        ];

        return view('scrn', $data);
    }
}
