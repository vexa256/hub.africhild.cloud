<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class UpdateLogic extends Controller
{
    public function SubTopicsFix(Request $request)
    {

        # code...

        $request->validate([
            '*' => 'required',

        ]);

        $TopicID = $request->TopicID;

        $Theme = DB::table('topics')->where('TopicID', $TopicID)->first();

        $ThemeID = $Theme->ThemeID;

        DB::table($request->TableName)->insert(
            $request->except([
                '_token',
                'TableName',
                'id',
            ])
        );

        $this->UploadFile(

            $request->Thumbnail, $request->TableName, 'Thumbnail', $request->uuid

        );

        DB::table('sub_topics')->where('uuid', $request->uuid)->update([

            "ThemeID" => $ThemeID,

        ]);

        return redirect()->back()->with('status', 'Record created successfully');

    }

    public function MassDelete($id, $TableName)
    {

        if (Schema::hasColumn($TableName, 'Thumbnail')) {

            $del = DB::table($TableName)->where('id', $id)->first();
            DB::table($TableName)->where('id', $id)->delete();

            unlink(public_path($del->Thumbnail));

            return redirect()->back()->with('status', 'The selected record was deleted successfully');

        } elseif (Schema::hasColumn($TableName, 'AttachmentFile')) {

            $del = DB::table($TableName)->where('id', $id)->first();

            unlink(public_path($del->AttachmentFile));

            DB::table($TableName)->where('id', $id)->delete();

            return redirect()->back()->with('status', 'The selected record was deleted successfully');

        } else {

            DB::table($TableName)->where('id', $id)->delete();
            return redirect()->back()->with('status', 'The selected record was deleted successfully');

        }

    }

    public function AutoInsert(Request $request)
    {
        if ('users' == $request->TableName) {

            $request->validate([
                '*'     => 'required',
                'email' => 'required|unique:users',

            ]);

            DB::table($request->TableName)->insert(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            DB::table($request->TableName)->where('UID', $request->UID)->update(
                [
                    'password' => \Hash::make($request->password),
                ]);

            return redirect()->back()->with('status', 'Admin created successfully');
        }
        if ($request->has('AttachmentFile') && $request->has('Thumbnail')) {

            $request->validate([
                '*'              => 'required',
                'Thumbnail'      => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4040',
                'AttachmentFile' => 'required|file|max:50048',

            ]);

            DB::table($request->TableName)->insert(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            $this->UploadFile(

                $request->Thumbnail, $request->TableName, 'Thumbnail', $request->uuid

            );

            $this->UploadFile(

                $request->AttachmentFile, $request->TableName, 'AttachmentFile', $request->uuid

            );

            return redirect()->back()->with('status', 'Record created successfully');

        } elseif ($request->has('Thumbnail')) {

            $request->validate([
                '*'         => 'required',
                'Thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:49040',
                // 'AttachmentFile' => 'nullable|file|mimes:pdf|max:20048',

            ]);

            DB::table($request->TableName)->insert(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            $this->UploadFile(

                $request->Thumbnail, $request->TableName, 'Thumbnail', $request->uuid

            );

            return redirect()->back()->with('status', 'Record created successfully');

        } elseif ($request->has('AttachmentFile')) {

            $request->validate([
                //'Thumbnail'      => 'nullable|image|max:4040',
                'AttachmentFile' => 'required|file|max:50048',

            ]);

            DB::table($request->TableName)->insert(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            $this->UploadFile(

                $request->AttachmentFile, $request->TableName, 'AttachmentFile', $request->uuid

            );

            return redirect()->back()->with('status', 'Record created successfully');

        } else {

            DB::table($request->TableName)->insert(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            return redirect()->back()->with('status', 'Record created successfully');

        }

    }

    public function AutoUpdate(Request $request)
    {

        if ('users' == $request->TableName) {

            $request->validate([
                '*' => 'required',

            ]);

            DB::table($request->TableName)->where('id', $request->id)->update(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            DB::table($request->TableName)->where('UID', $request->UID)->update(
                [
                    'password' => \Hash::make($request->password),
                ]);

            return redirect()->back()->with('status', 'Admin created successfully');
        }

        if ($request->has('Thumbnail')) {

            $request->validate([
                '*'         => 'required',
                'Thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10040',
                // 'AttachmentFile' => 'nullable|file|mimes:pdf|max:20048',

            ]);

            $up = DB::table($request->TableName)->where('id', $request->id)->first();

            unlink(public_path($up->Thumbnail));

            DB::table($request->TableName)->where('id', $request->id)->update(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            $this->UpdateFile(

                $request->Thumbnail, $request->TableName, 'Thumbnail', $request->uuid, $request->id

            );

            return redirect()->back()->with('status', 'Record created successfully');

        } elseif ($request->has('AttachmentFile')) {

            $request->validate([
                //'Thumbnail'      => 'nullable|image|max:4040',
                'AttachmentFile' => 'required|file|max:50048',

            ]);

            $up = DB::table($request->TableName)->where('id', $request->id)->first();

            unlink(public_path($up->AttachmentFile));

            DB::table($request->TableName)->where('id', $request->id)->update(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            $this->UpdateFile(

                $request->AttachmentFile, $request->TableName, 'AttachmentFile', $request->uuid, $request->id

            );

            return redirect()->back()->with('status', 'Record created successfully');

        } else {
            $request->validate([
                //'Thumbnail'      => 'nullable|image|max:4040',
                '*' => 'required',

            ]);

            DB::table($request->TableName)->where('id', $request->id)->update(
                $request->except([
                    '_token',
                    'TableName',
                    'id',
                ])
            );

            return redirect()->back()->with('status', 'Record updated successfully');

        }

    }

    public function UploadFile($File, $TableName, $Col, $uuid)
    {
        $FileName = time() . '.' . $File->extension();

        $File->move(public_path('assets/docs'), $FileName);

        DB::table($TableName)->where('uuid', $uuid)->update([

            $Col => 'assets/docs/' . $FileName,
        ]);
    }

    public function UpdateFile($File, $TableName, $Col, $uuid, $id)
    {
        $FileName = time() . '.' . $File->extension();

        $File->move(public_path('assets/docs'), $FileName);

        DB::table($TableName)->where('id', $id)->update(
            [
                $Col => 'assets/docs/' . $FileName,
            ]
        );

    }
}
