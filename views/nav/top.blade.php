   <!--begin::Topbar-->
   <div class="d-flex align-items-stretch flex-shrink-0">
       <!--begin::Toolbar wrapper-->
       <div class="d-flex align-items-stretch flex-shrink-0">
           <!--begin::Search-->
           <div class="d-flex align-items-stretch ms-1 ms-lg-3">
               <!--begin::Search-->
               <div id="kt_header_search" class="d-none align-items-stretch"
                   data-kt-search-keypress="true" data-kt-search-min-length="2"
                   data-kt-search-enter="enter" data-kt-search-layout="menu"
                   data-kt-menu-trigger="auto" data-kt-menu-overflow="false"
                   data-kt-menu-permanent="true" data-kt-menu-placement="bottom-end"
                   data-kt-menu-flip="bottom">
                   <!--begin::Search toggle-->
                   <div class="d-flex align-items-center" data-kt-search-element="toggle"
                       id="kt_header_search_toggle">
                       <select class="form-select" data-control="select2"
                           data-placeholder="Search all files in storage">
                           <option></option>
                           <option value="1">Option 1</option>
                           <option value="2">Option 2</option>
                       </select>

                   </div>
                   <!--end::Search toggle-->

               </div>
               <!--end::Search-->
           </div>
           <!--end::Search-->


           <!--begin::User-->
           <div class="d-flex align-items-center ms-1 ms-lg-3"
               id="kt_header_user_menu_toggle">
               <!--begin::Menu wrapper-->
               <div class="cursor-pointer symbol symbol-30px symbol-md-40px"
                   data-kt-menu-trigger="click" data-kt-menu-attach="parent"
                   data-kt-menu-placement="bottom-end" data-kt-menu-flip="bottom">
                   <i class="fas fa-users fa-3x text-danger" aria-hidden="true"></i>
               </div>
               <!--begin::Menu-->
               <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px"
                   data-kt-menu="true">
                   <!--begin::Menu item-->
                   <div class="menu-item px-3">
                       <div class="menu-content d-flex align-items-center px-3">
                           <!--begin::Avatar-->
                           <div class="symbol symbol-50px me-5">
                               <img alt="Logo" src="{{ asset('ava.jpg') }}" />
                           </div>
                           <!--end::Avatar-->
                           <!--begin::Username-->
                           <div class="d-flex flex-column">
                               <div class="fw-bolder d-flex align-items-center fs-5">
                                   @auth
                                       {{ Auth::user()->name }}
                                   @endauth

                               </div>
                               <a href="#"
                                   class="fw-bold text-muted text-hover-primary fs-7">
                                   @auth
                                       {{ Auth::user()->email }}

                                   @endauth
                               </a>
                           </div>
                           <!--end::Username-->
                       </div>
                   </div>
                   <!--end::Menu item-->




                   <!--begin::Menu separator-->
                   <div class="separator my-2"></div>
                   <!--end::Menu separator-->
                   <!--begin::Menu item-->
                   <div class="menu-item px-5" data-kt-menu-trigger="hover"
                       data-kt-menu-placement="left-start" data-kt-menu-flip="bottom">
                       <a href="#" class="menu-link px-5">
                           <span class="menu-title position-relative">Language
                               <span
                                   class="fs-8 rounded bg-light px-3 py-2 position-absolute translate-middle-y top-50 end-0">English
                                   <img class="w-15px h-15px rounded-1 ms-2"
                                       src="{{ asset('assets/media/flags/united-states.svg') }}"
                                       alt="metronic" /></span></span>
                       </a>

                   </div>



                   <form id="logout-form" action="{{ route('logout') }}" method="POST"
                       style="display: none;">
                       @csrf
                   </form>
                   <!--end::Menu item-->
                   <!--begin::Menu item-->
                   <div class="menu-item px-5 my-1">
                       <a href="#UpdateAccount" data-bs-toggle="modal"
                           class="menu-link px-5">Update Credentials</a>
                   </div>
                   <!--end::Menu item-->
                   <!--begin::Menu item-->
                   <div class="menu-item px-5">
                       <a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                           class="menu-link px-5">Sign
                           Out</a>
                   </div>
                   <!--end::Menu item-->
               </div>
               <!--end::Menu-->
               <!--end::Menu wrapper-->
           </div>
           <!--end::User -->
           <!--begin::Heaeder menu toggle-->
           <div class="d-flex align-items-center d-lg-none ms-2" title="Show header menu">
               <div class="btn btn-icon btn-active-color-primary w-30px h-30px w-md-40px h-md-40px"
                   id="kt_header_menu_mobile_toggle">
                   <!--begin::Svg Icon | path: icons/duotone/Text/Toggle-Right.svg-->
                   <span class="svg-icon svg-icon-1">
                       <svg xmlns="http://www.w3.org/2000/svg"
                           xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                           height="24px" viewBox="0 0 24 24" version="1.1">
                           <g stroke="none" stroke-width="1" fill="none"
                               fill-rule="evenodd">
                               <rect x="0" y="0" width="24" height="24" />
                               <path fill-rule="evenodd" clip-rule="evenodd"
                                   d="M22 11.5C22 12.3284 21.3284 13 20.5 13H3.5C2.6716 13 2 12.3284 2 11.5C2 10.6716 2.6716 10 3.5 10H20.5C21.3284 10 22 10.6716 22 11.5Z"
                                   fill="black" />
                               <path opacity="0.5" fill-rule="evenodd" clip-rule="evenodd"
                                   d="M14.5 20C15.3284 20 16 19.3284 16 18.5C16 17.6716 15.3284 17 14.5 17H3.5C2.6716 17 2 17.6716 2 18.5C2 19.3284 2.6716 20 3.5 20H14.5ZM8.5 6C9.3284 6 10 5.32843 10 4.5C10 3.67157 9.3284 3 8.5 3H3.5C2.6716 3 2 3.67157 2 4.5C2 5.32843 2.6716 6 3.5 6H8.5Z"
                                   fill="black" />
                           </g>
                       </svg>
                   </span>
                   <!--end::Svg Icon-->
               </div>
           </div>
           <!--end::Heaeder menu toggle-->
       </div>
       <!--end::Toolbar wrapper-->
   </div>
   <!--end::Topbar-->
