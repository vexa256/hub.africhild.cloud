 <!--begin::Header-->
 <div id="kt_header" style="" class="header align-items-stretch">
     <!--begin::Container-->
     <div class="container-fluid d-flex align-items-stretch justify-content-between">

         @include('mobile.mobile')
         <!--begin::Wrapper-->
         <div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1">

             @include('nav.nav')
             @include('nav.top')

         </div>
         <!--end::Wrapper-->
     </div>
     <!--end::Container-->
 </div>
 <!--end::Header-->
 <!--begin::Toolbar-->
