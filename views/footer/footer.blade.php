 <!--begin::Footer-->
 <div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
     <!--begin::Container-->
     <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
         <!--begin::Copyright-->
         <div class="text-dark order-2 order-md-1">
             <span class="text-muted fw-bold me-1">{{ date('Y') }}©</span>
             <a href="" target="_blank" class="text-gray-800 text-hover-primary">AfriChild</a>
         </div>
         <!--end::Copyright-->
         <!--begin::Menu-->
         <ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
             <li class="menu-item">
                 <a href="https://africhild.or.ug/Home" target="_blank" class="menu-link px-2">AfriChild Website</a>
             </li>

         </ul>
         <!--end::Menu-->
     </div>
     <!--end::Container-->
 </div>
 <!--end::Footer-->

 @auth
     <div class="modal bg-white fade" tabindex="-1" id="UpdateAccount">
         <div class="modal-dialog modal-lg">
             <div class="modal-content shadow-none">
                 <div class="modal-header">
                     <h5 class="modal-title">Hello, {{ Auth::user()->name }}, Update your account details</h5>

                     <!--begin::Close-->
                     <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                         aria-label="Close">
                         <span class="svg-icon svg-icon-2x"></span>
                     </div>
                     <!--end::Close-->
                 </div>

                 <form action="{{ route('AutoUpdate') }}" class="row" method="POST"
                     enctype="multipart/form-data">
                     @csrf

                     <div class="modal-body">

                         <div class="row">

                             <div class="col-md-4">
                                 <div class="mb-5">
                                     <label for="" class="required form-label">Name</label>
                                     <input value="{{ Auth::user()->name }}" required type="text"
                                         class="form-control form-control-solid" name="name" />
                                 </div>
                             </div>

                             <div class="col-md-4">
                                 <div class="mb-5">
                                     <label for="" class="required form-label">Email</label>
                                     <input value="{{ Auth::user()->email }}" required type="text"
                                         class="form-control form-control-solid" name="email" />
                                 </div>
                             </div>

                             <div class="col-md-4">
                                 <div class="mb-5">
                                     <label for="" class="required form-label">Phone</label>
                                     <input value="{{ Auth::user()->Phone }}" required type="text"
                                         class="form-control form-control-solid" name="Phone" />
                                 </div>
                             </div>

                             <div class="col-md-4">
                                 <div class="mb-5">
                                     <label for="" class="required form-label">Nationality</label>
                                     <input value="{{ Auth::user()->Nationality }}" required type="text"
                                         class="form-control form-control-solid" name="Nationality" />
                                 </div>
                             </div>

                             <div class="col-md-4">
                                 <div class="mb-5">
                                     <label for="" class="required form-label">Password</label>
                                     <input value="" required type="text" class="form-control form-control-solid"
                                         name="pasword" />
                                 </div>
                             </div>

                             <input type="hidden" name="id" value="{{ Auth::user()->id }}">
                             <input type="hidden" name="UID" value="{{ Auth::user()->UID }}">
                             <input type="hidden" name="TableName" value="users">

                             <div class="col-md-4">
                                 <label for="" class="required form-label">Sex</label>
                                 <select required name="Sex" class="form-select" data-control="select2"
                                     data-placeholder="Select an option">
                                     <option value="{{ Auth::user()->Sex }}">{{ Auth::user()->Sex }}</option>
                                     <option value="Female">Female</option>
                                     <option value="Male"> Male</option>


                                 </select>
                             </div>

                             <div class="col-md-4">
                                 <div class="mb-5">
                                     <label for="" class="required form-label">Proffession</label>
                                     <input value="{{ Auth::user()->Proffession }}" required type="text"
                                         class="form-control form-control-solid" name="Proffession" />
                                 </div>
                             </div>
                         </div>



                     </div>

                     <div class="modal-footer">
                         <button type="button" class="btn btn-dark" data-bs-dismiss="modal">Close</button>
                         <button type="submit" class="btn btn-danger">Save changes</button>
                     </div>

                 </form>
             </div>
         </div>
     </div>
 @endauth
