<!DOCTYPE html>

<html lang="en">

<head>

    <title>

    </title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="utf-8" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <link rel="canonical" href="" />
    <link rel="shortcut icon" href="" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dt/datatables.min.css') }}" />




    <style>
        .bi-2x {

            height: 20px !important;
            width: 20px !important;

        }

    </style>
    <style>
        .btn-group-sm>.btn,
        .btn-sm {
            padding: .25rem .4rem;
            font-size: .875rem;
            line-height: .5;
            border-radius: .2rem;
        }

        .table-pad {

            padding-top: 3px !important;
        }

        th {

            background-color: #181c32 !important;
            color: white !important;
            font-weight: 500 !important;
        }

        table,
        th,
        td {
            /*   border: 0.5px solid !important;*/
        }

        .tox-tinymce {

            height: 450px !important;
        }

        tr,
        td,
        th,
        tbody,
        thead {

            font-size: 10px !important;


        }

    </style>
    @auth
    @if (Auth::user()->Role != 'SuperAdmin')
        <style>
            .deleteConfirm {
                display: none !important;
            }

        </style>
    @endif
  @else
  
@endauth
   
</head>
<!--end::Head-->
<!--begin::Body-->

<body id="kt_body"
    class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed">
