<script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
<script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/dt/datatables.min.js') }}"></script>
<script src="{{ asset('assets/tiny/tinymce.min.js') }}"></script>
<script defer src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
<script src="{{ asset('assets/plugins/custom/fslightbox/fslightbox.bundle.js') }}"></script>


<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>

<script>
$( document ).ready(function() {
   $( ".lock" ).click(function() {
  Swal.fire('Coming Soon', 'Trainings and Courses will be available in the near future', 'success');
});
});

</script>

@include('not.not')
</body>
<!--end::Body-->

</html>
