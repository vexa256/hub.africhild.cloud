@include('header.header')
<!--begin::Main-->
<!--begin::Root-->
<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="page d-flex flex-row flex-column-fluid">

        @include('sidebar.sidebar')
        <!--begin::Wrapper-->
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
            @include('nav.main')
            @include('nav.tool')

            <!--end::Toolbar-->
            @include('body.body')
            @include('footer.footer')

        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>

@include('footer.modals')

@include('scripts.scripts')
