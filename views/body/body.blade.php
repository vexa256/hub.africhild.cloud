   <!--begin::Content-->
   <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
       <!--begin::Container-->
       <div id="kt_content_container" class="container-fluid">

           <div class="row">
               @isset($Page)
                   @include($Page)
               @endisset

           </div>
       </div>
       <!--end::Container-->
   </div>
   <!--end::Content-->
