{{-- <div data-kt-menu-trigger="click" data-kt-menu-placement="right-start" class="menu-item py-2">
    <span class="menu-link menu-center" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
        <span class="menu-icon me-0">
            <i class="fas fa-chalkboard text-danger fs-2"></i>
        </span>
        <span class="menu-title">Trainings</span>
    </span>
    <div class="menu-sub menu-sub-dropdown w-225px px-1 py-4">
        <div class="menu-item">
            <div class="menu-content">
                <span class="menu-section fs-5 fw-bolder ps-1 py-1">Training options</span>
            </div>
        </div>


        <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="right-start"
            class="menu-item menu-accordion">
            <span class="menu-link">
                <span class="menu-bullet">
                    <span class="fas fa-dot-circle text-danger"></span>
                </span>
                <span class="menu-title">Trainings</span>
                <span class="menu-arrow"></span>
            </span>
            <div class="menu-sub menu-sub-accordion menu-sub-lg-dropdown w-lg-225px px-lg-1 py-lg-4">
                @if (Auth::user()->Role == 'Admin' || Auth::user()->Role == 'SuperAdmin')
                    {{ MenuLink($Label = 'Manage Trainings', $Link = route('MgtTrainings')) }}
                @endif
                {{ MenuLink($Label = 'View Trainings', $Link = route('ViewTrainings')) }}


            </div>
        </div>
        @if (Auth::user()->Role == 'Admin' || Auth::user()->Role == 'SuperAdmin')
            <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="right-start"
                class="menu-item menu-accordion">
                <span class="menu-link">
                    <span class="menu-bullet">
                        <span class="fas fa-dot-circle text-danger"></span>
                    </span>
                    <span class="menu-title">Training Materials</span>
                    <span class="menu-arrow"></span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-sub-lg-dropdown w-lg-225px px-lg-1 py-lg-4">


                    {{ MenuLink($Label = 'Training Videos', $Link = route('SelectVideo')) }}
                    {{ MenuLink($Label = 'Training Notes', $Link = route('SelectNotes')) }}
                    {{ MenuLink($Label = 'Training Documents', $Link = route('SelectDocs')) }}


                </div>
            </div>
        @endif
        @if (Auth::user()->Role == 'Admin' || Auth::user()->Role == 'SuperAdmin')
            <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="right-start"
                class="menu-item menu-accordion d-none">
                <span class="menu-link">
                    <span class="menu-bullet">
                        <span class="fas fa-dot-circle text-danger"></span>
                    </span>
                    <span class="menu-title"> Quiz and Feedback</span>
                    <span class="menu-arrow"></span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-sub-lg-dropdown w-lg-225px px-lg-1 py-lg-4">
                    {{ MenuLink($Label = 'Create Quiz', $Link = route('SelectTrainingQuiz')) }}
                    {{ MenuLink($Label = 'Quiz Questions', $Link = route('SelectQuiz')) }}
                    {{ MenuLink($Label = 'Trainee Feedback', $Link = route('SelectFeed')) }}
                </div>
            </div>
        @endif
        @if (Auth::user()->Role == 'Admin' || Auth::user()->Role == 'SuperAdmin')
            <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="right-start"
                class="menu-item menu-accordion">
                <span class="menu-link">
                    <span class="menu-bullet">
                        <span class="fas fa-dot-circle text-danger"></span>
                    </span>
                    <span class="menu-title"> Reports</span>
                    <span class="menu-arrow"></span>
                </span>
                <div class="menu-sub menu-sub-accordion menu-sub-lg-dropdown w-lg-225px px-lg-1 py-lg-4">
                    {{ MenuLink($Label = 'Training Reports', $Link = route('ReportTraining')) }}

                </div>
            </div>
        @endif

    </div>
</div> --}}
