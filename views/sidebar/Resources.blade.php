@auth
    @if (Auth::user()->Role == 'Admin' || Auth::user()->Role == 'SuperAdmin')
        <div data-kt-menu-trigger="click" data-kt-menu-placement="right-start"
            class="menu-item py-2">
            <span class="menu-link menu-center" data-bs-trigger="hover" data-bs-dismiss="click"
                data-bs-placement="right">
                <span class="menu-icon me-0">
                    <i class="fas fa-file-signature text-danger fs-2"></i>
                </span>
                <span class="menu-title">Settings</span>
            </span>
            <div class="menu-sub menu-sub-dropdown w-225px px-1 py-4">
                <div class="menu-item">
                    <div class="menu-content">
                        <span class="menu-section fs-5 fw-bolder ps-1 py-1">AfriChild
                            Resources</span>
                    </div>
                </div>


                <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}"
                    data-kt-menu-placement="right-start" class="menu-item menu-accordion">
                    <span class="menu-link">
                        <span class="menu-bullet">
                            <span class="fas fa-dot-circle text-danger"></span>
                        </span>
                        <span class="menu-title">Content Settings</span>
                        <span class="menu-arrow"></span>
                    </span>
                    <div
                        class="menu-sub menu-sub-accordion menu-sub-lg-dropdown w-lg-225px px-lg-1 py-lg-4">
                        {{ MenuLink($Label = 'Manage Themes', $Link = route('MgtThemes')) }}
                        {{ MenuLink($Label = 'Manage Topics', $Link = route('MgtTopics')) }}
                        {{ MenuLink($Label = 'Manage Sub-Topics', $Link = route('MgtSubTopics')) }}
                        {{ MenuLink($Label = 'Manage Resources', $Link = route('MgtMainResources')) }}


                    </div>

                </div>




            </div>
        </div>
    @endif
@endauth


@guest

    <div data-kt-menu-trigger="click" data-kt-menu-placement="right-start"
        class="menu-item py-2">
        <span class="menu-link menu-center" data-bs-trigger="hover" data-bs-dismiss="click"
            data-bs-placement="right">
            <span class="menu-icon me-0">
                <i class="fas fa-store-alt text-danger fs-2"></i>
            </span>
            <span class="menu-title">library</span>
        </span>
        <div class="menu-sub menu-sub-dropdown w-225px px-1 py-4">
            <div class="menu-item">
                <div class="menu-content">
                    <span class="menu-section fs-5 fw-bolder ps-1 py-1">AfriChild's
                        Resources</span>
                </div>
            </div>


            <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}"
                data-kt-menu-placement="right-start" class="menu-item menu-accordion">
                <span class="menu-link">
                    <span class="menu-bullet">
                        <span class="fas fa-dot-circle text-danger"></span>
                    </span>
                    <span class="menu-title"> Resources</span>
                    <span class="menu-arrow"></span>
                </span>
                <div
                    class="menu-sub menu-sub-accordion menu-sub-lg-dropdown w-lg-225px px-lg-1 py-lg-4">
                    {{ MenuLink($Label = 'library', $Link = route('ViewThemes')) }}



                </div>

            </div>




        </div>
    </div>

@endguest

<div data-kt-menu-trigger="click" data-kt-menu-placement="right-start"
    class="menu-item py-2">
    <span class="menu-link menu-center" data-bs-trigger="hover" data-bs-dismiss="click"
        data-bs-placement="right">
        <span class="menu-icon me-0">
            <i class="fas fas fa-comment-dots text-danger fs-2"></i>
        </span>
        <span class="menu-title">Forum</span>
    </span>
    <div class="menu-sub menu-sub-dropdown w-225px px-1 py-4">
        <div class="menu-item">
            <div class="menu-content">
                <span class="menu-section fs-5 fw-bolder ps-1 py-1">AfriChild's
                    Forum</span>
            </div>
        </div>


        <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}"
            data-kt-menu-placement="right-start" class="menu-item menu-accordion">
            <span class="menu-link">
                <span class="menu-bullet">
                    <span class=" fas fa-comment-dots text-danger"></span>
                </span>
                <span class="menu-title">Community Forum</span>
                <span class="menu-arrow"></span>
            </span>
            <div
                class="menu-sub menu-sub-accordion menu-sub-lg-dropdown w-lg-225px px-lg-1 py-lg-4">
                {{ MenuLink($Label = 'Forum', $Link = 'https://chat.africhild.cloud') }}



            </div>

        </div>




    </div>
</div>


<div data-kt-menu-trigger="click" data-kt-menu-placement="right-start"
    class="menu-item py-2">
    <span class="menu-link menu-center" data-bs-trigger="hover" data-bs-dismiss="click"
        data-bs-placement="right">
        <span class="menu-icon me-0">
            <i class="fas fas fa-book text-danger fs-2"></i>
        </span>
        <span class="menu-title">Trainings</span>
    </span>
    <div class="menu-sub menu-sub-dropdown w-225px px-1 py-4">
        <div class="menu-item">
            <div class="menu-content">
                <span class="menu-section fs-5 fw-bolder ps-1 py-1">AfriChild's
                    Trainings</span>
            </div>
        </div>


        <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}"
            data-kt-menu-placement="right-start" class="menu-item menu-accordion">
            <span class="menu-link">
                <span class="menu-bullet">
                    <span class=" fas fa-comment-dots text-danger"></span>
                </span>
                <span class="menu-title">Trainings</span>
                <span class="menu-arrow"></span>
            </span>
            <div
                class="menu-sub menu-sub-accordion menu-sub-lg-dropdown w-lg-225px px-lg-1 py-lg-4">
                {{ MenuLink($Label = 'Courses', $Link = '#', $class="lock") }}



            </div>

        </div>




    </div>
</div>
