    <!--begin::Aside-->
    <div id="kt_aside" class="aside pb-5 pt-5 pt-lg-0" data-kt-drawer="true"
        data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}"
        data-kt-drawer-overlay="true"
        data-kt-drawer-width="{default:'80px', '300px': '100px'}"
        data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
        <!--begin::Brand-->
        <div class="aside-logo py-8" id="kt_aside_logo">
            <!--begin::Logo-->
            <a href="#" class="d-flex align-items-center">
                <img alt="Logo" src="{{ asset('logo2.png') }}" class="h-45px logo" />
            </a>
            <!--end::Logo-->
        </div>
        <!--end::Brand-->
        <!--begin::Aside menu-->
        <div class="aside-menu flex-column-fluid" id="kt_aside_menu">
            <!--begin::Aside Menu-->
            <div class="hover-scroll-overlay-y my-2 my-lg-5 pe-lg-n1"
                id="kt_aside_menu_wrapper" data-kt-scroll="true"
                data-kt-scroll-height="auto"
                data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer"
                data-kt-scroll-wrappers="#kt_aside, #kt_aside_menu"
                data-kt-scroll-offset="5px">
                <!--begin::Menu-->
                <div class="menu menu-column menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500 fw-bold"
                    id="#kt_aside_menu" data-kt-menu="true">
                    <div class="menu-item py-2">
                        <a class="menu-link menu-center" href="{{ route('home') }}"
                            data-bs-trigger="hover" data-bs-dismiss="click"
                            data-bs-placement="right">
                            <span class="menu-icon me-0">
                                <i class="fa fa-home text-danger fs-2"></i>
                            </span>
                            <span class="menu-title">Home</span>
                        </a>
                    </div>

                    @include('sidebar.Trainings')
                    @include('sidebar.Resources')

                </div>
                <!--end::Menu-->
            </div>
            <!--end::Aside Menu-->
        </div>
        <!--end::Aside menu-->
        <!--begin::Footer-->
        <div class="aside-footer flex-column-auto" id="kt_aside_footer">
            <!--begin::Menu-->
            <div class="d-flex justify-content-center">
                <button type="button" class="btn btm-sm btn-icon btn-active-color-primary"
                    data-kt-menu-trigger="click" data-kt-menu-overflow="true"
                    data-kt-menu-placement="top-start" data-kt-menu-flip="top-end"
                    data-bs-toggle="tooltip" data-bs-placement="right"
                    data-bs-dismiss="click" title="Admin Settings">
                    <!--begin::Svg Icon | path: icons/duotone/Communication/Dial-numbers.svg-->
                    <i style="font-size: 40px" class="fas fa-3x text-primary fa-cogs"
                        aria-hidden="true"></i>
                    <!--end::Svg Icon-->
                </button>
                <!--begin::Menu 2-->
                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px"
                    data-kt-menu="true">
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">
                            Settings
                        </div>
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu separator-->
                    <div class="separator mb-3 opacity-75"></div>
                    <!--end::Menu separator-->
                    <!--begin::Menu item-->
                    @auth
                        @if (Auth::user()->Role == 'Admin' || Auth::user()->Role == 'SuperAdmin')
                            <div class="menu-item px-3">
                                <a href="{{ route('MgtUsers') }}"
                                    class="menu-link px-3">Manage Users</a>
                            </div>
                            <div class="menu-item px-3">
                                <a href="{{ route('AdminSettings') }}"
                                    class="menu-link px-3"> Admin Settings</a>
                            </div>
                        @endif
                    @endauth

                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">Logout</a>
                    </div>
                    <!--end::Menu item-->




                </div>
                <!--end::Menu 2-->
            </div>
            <!--end::Menu-->
        </div>
        <!--end::Footer-->
    </div>
    <!--end::Aside-->
