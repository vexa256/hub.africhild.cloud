<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->id();
            $table->string('ThemeID');
            $table->string('TopicID');
            $table->string('SubTopicID');
            $table->string('ResourceTitle');
            $table->string('CID')->nullable();
            $table->string('uuid');
            $table->string('Thumbnail');
            $table->string('AttachmentFile');
            $table->string('BriefDescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources');
    }
};
