<div class="row">
    <div class=" float-end">
        {{ HeaderBtn($Toggle = 'New', $Class = 'btn-dark btn-sm', $Label = 'New Theme ', $Icon = 'fa-plus') }}
    </div>
</div>
<div class="card-body pt-3 bg-light shadow-lg table-responsive">
    {!! Alert($icon = 'fa-info', $class = 'alert-danger', $Title = 'AfriChild Content  Knowledge Hub Themes', $Msg = 'Manage Resource Themes') !!}
</div>

<div class="card-body pt-3 bg-light shadow-lg table-responsive">

    <table id="kt_datatable_example_2"
        class="mytable table table-striped table-row-bordered gy-5 gs-7">
        <thead>
            <tr class="fw-bolder fs-3 text-dark">
                <th>Theme</th>
                <th>Description</th>
                <th class="bg-danger text-light fw-bolder">Thumbnail</th>
                <th>Update</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>

            @isset($Themes)
                @foreach ($Themes as $data)
                    <tr>

                        <td>{{ $data->Title }}</td>
                        <td>{{ $data->Description }}</td>
                        {{-- <td>{{ $data->BriefDescription }}</td> --}}
                        <td>
                            <a data-fslightbox="lightbox-basic"
                                href="{{ asset($data->Thumbnail) }}"
                                class="btn btn-sm btn-dark">
                                <i class="fas fa-binoculars" aria-hidden="true"></i>
                            </a>
                        </td>

                        <td>
                            <a data-bs-toggle="modal" href="#Update{{ $data->id }}"
                                class="btn btn-sm btn-dark">
                                <i class="fas fa-edit" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            {!! ConfirmBtn(
    $data = [
        'msg' => 'You want to delete this record',
        'route' => route('MassDelete', ['id' => $data->id, 'TableName' => 'themes']),
        'label' => '<i class="fas fa-trash"></i>',
        'class' => 'btn btn-danger btn-sm deleteConfirm',
    ],
) !!}
                        </td>
                    </tr>
                @endforeach
            @endisset



        </tbody>

    </table>




</div>


@isset($Themes)
    @foreach ($Themes as $data)
        {{ UpdateModalHeader($Title = 'Update the selected  record', $ModalID = $data->id) }}
        <form novalidate action="{{ route('AutoUpdate') }}" class="" method="POST"
            enctype="multipart/form-data">
            @csrf

            <div class="row">

                <div class="col-md-12">
                    <div class="mb-5">
                        <label for="" class="required form-label">Thumbnail
                        </label>
                        <input required type="file" class="form-control form-control-solid"
                            name="Thumbnail" />
                    </div>
                </div>


                <input type="hidden" name="id" value="{{ $data->id }}">

                <input type="hidden" name="TableName" value="themes">

                {{ RunUpdateModalFinal($ModalID = $data->id, $Extra = '', $csrf = null, $Title = null, $RecordID = $data->id, $col = '6', $te = '12', $TableName = 'themes') }}
            </div>


            {{ UpdateModalFooter() }}

        </form>
    @endforeach
@endisset



@include('Themes.NewTheme')
{{-- @include('PDF.PDFJS') --}}
