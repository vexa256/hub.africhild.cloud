<div class="row">
    <div class=" float-end">
        {{ HeaderBtn($Toggle = 'New', $Class = 'btn-dark btn-sm', $Label = 'New Theme Sub-Topics ', $Icon = 'fa-plus') }}
    </div>
</div>
<div class="card-body pt-3 bg-light shadow-lg table-responsive">
    {!! Alert(
        $icon = 'fa-info',
        $class = 'alert-danger',
        $Title = 'AfriChild Content  Knowledge Hub Theme Sub-Topics',
        $Msg = 'Manage Resource Sub-Topics',
    ) !!}
</div>

<div class="card-body pt-3 bg-light shadow-lg table-responsive">

    <table id="kt_datatable_example_2"
        class="mytable table table-striped table-row-bordered gy-5 gs-7">
        <thead>
            <tr class="fw-bolder fs-3 text-dark">
                {{-- <th>Theme</th> --}}
                <th>Topic</th>
                <th>Sub-Topic</th>
                <th>Description</th>
                <th class="bg-danger text-light fw-bolder">Thumbnail</th>
                <th>Update</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>

            @isset($SubTopics)
                @foreach ($SubTopics as $data)
                    <tr>

                        {{-- <td>{{ $data->Theme }}</td> --}}
                        <td>{{ $data->Topic }}</td>
                        <td>{{ $data->Title }}</td>
                        <td>{!! $data->Description !!}</td>
                        {{-- <td>{{ $data->BriefDescription }}</td> --}}
                        <td>
                            <a data-fslightbox="lightbox-basic"
                                href="{{ asset($data->Thumbnail) }}"
                                class="btn btn-sm btn-dark">
                                <i class="fas fa-binoculars" aria-hidden="true"></i>
                            </a>
                        </td>

                        <td>
                            <a data-bs-toggle="modal"
                                href="#Update{{ $data->id }}"
                                class="btn btn-sm btn-dark">
                                <i class="fas fa-edit" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            {!! ConfirmBtn(
                                $data = [
                                    'msg' => 'You want to delete this record',
                                    'route' => route('MassDelete', [
                                        'id' => $data->id,
                                        'TableName' => 'sub_topics',
                                    ]),
                                    'label' => '<i class="fas fa-trash"></i>',
                                    'class' => 'btn btn-danger btn-sm deleteConfirm',
                                ],
                            ) !!}
                        </td>
                    </tr>
                @endforeach
            @endisset



        </tbody>

    </table>




</div>


@isset($SubTopics)
    @foreach ($SubTopics as $data)
        {{ UpdateModalHeader($Title = 'Update the selected  record', $ModalID = $data->id) }}
        <form novalidate action="{{ route('AutoUpdate') }}" class=""
            method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row">

                <div class="col-md-12">
                    <div class="mt-3 mb-4">
                        <label for="" class="required form-label">Parent
                            Topic</label>

                        <select name="ThemeID" class="form-select"
                            data-control="select2"
                            data-placeholder="Select an option">
                            <option value="{{ $data->TopicID }}">
                                {{ $data->Topic }}</option>

                            @isset($Topics)
                                @foreach ($Topics as $dat)
                                    <option value="{{ $dat->TopicID }}">
                                        {{ $dat->Title }}</option>
                                @endforeach
                            @endisset

                        </select>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="mb-5">
                        <label for="" class="required form-label">Thumbnail
                        </label>
                        <input required type="file"
                            class="form-control form-control-solid"
                            name="Thumbnail" />
                    </div>
                </div>


                <input type="hidden" name="id" value="{{ $data->id }}">

                <input type="hidden" name="TableName" value="sub_topics">

                {{ RunUpdateModalFinal($ModalID = $data->id, $Extra = '', $csrf = null, $Title = null, $RecordID = $data->id, $col = '6', $te = '12', $TableName = 'sub_topics') }}
            </div>


            {{ UpdateModalFooter() }}

        </form>
    @endforeach
@endisset



@include('SubTopics.NewSubTopic')
{{-- @include('PDF.PDFJS') --}}
