<div class="modal bg-white fade" id="New">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content shadow-none">
            <div class="modal-header">
                <h5 class="modal-title">Create new resource | theme sub-topic
                </h5>
                <form enctype="multipart/form-data"
                    action="{{ route('SubTopicsFix') }}" method="POST">
                    <div class="row">
                        <div class="col-12 float-end">
                            <button type="button"
                                class="btn btn-dark  float-end"
                                data-bs-dismiss="modal">Close</button>
                            <button type="submit"
                                class="btn  btn-danger mx-2 float-end">Save
                                changes</button>

                        </div>
                    </div>
            </div>


            @csrf
            {{-- modal body --}}
            {{-- modal body --}}
            {{-- modal body --}}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mb-5">
                            <label for=""
                                class="required form-label">Thumbnail
                            </label>
                            <input required type="file"
                                class="form-control form-control-solid"
                                name="Thumbnail" />
                        </div>
                    </div>
                    {{-- <div class="col-md-6">
                        <div class="mb-5">
                            <label for="" class="required form-label">Resource
                                File (PDF Only)</label>
                            <input required type="file"
                                class="form-control form-control-solid"
                                name="AttachmentFile" />
                        </div>
                    </div> --}}
                    <div class="col-md-12">
                        <div class="mt-3 mb-5">
                            <label for=""
                                class="required form-label">Parent
                                Topic</label>

                            <select name="TopicID" class="form-select"
                                data-control="select2"
                                data-placeholder="Select an option">
                                <option></option>

                                @isset($Topics)
                                    @foreach ($Topics as $data)
                                        <option value="{{ $data->TopicID }}">
                                            {{ $data->Title }}</option>
                                    @endforeach
                                @endisset

                            </select>
                        </div>
                    </div>

                    @foreach ($Form as $data)
                        @if ($data['type'] == 'string')
                            {{ CreateInputText($data, $placeholder = null, $col = '6') }}
                        @elseif ($data['type'] == 'integer')
                            {{ CreateInputInteger($data, $placeholder = null, $col = '3') }}
                        @elseif ($data['type'] == 'date' || $data['type'] == 'datetime')
                            {{ CreateInputDate($data, $placeholder = null, $col = '3') }}
                        @endif
                    @endforeach

                </div>

                <input required type="hidden" name="TableName"
                    value="sub_topics">

                <input required required type="hidden" name="created_at"
                    value="{{ date('Y-m-d H:i:s') }}">


                <input required type="hidden" name="SubTopicID"
                    value="{{ md5(uniqid() . 'AFC' . date('Y-m-d H:I:S')) }}">

                <input required type="hidden" name="uuid"
                    value="{{ md5(uniqid() . 'AFC' . date('Y-m-d H:I:S')) }}">


                <div class="row">
                    @foreach ($Form as $data)
                        @if ($data['type'] == 'text')
                            {{ CreateInputEditor($data, $placeholder = null, $col = '12') }}
                        @endif
                    @endforeach
                    </form>
                </div>
            </div>
            {{-- modal body --}}
            {{-- modal body --}}
            {{-- modal body --}}


        </div>
    </div>
</div>
