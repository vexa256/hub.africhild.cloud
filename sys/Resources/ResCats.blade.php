<div class="row">
    <div class=" float-end">
        {{ HeaderBtn($Toggle = 'New', $Class = 'btn-dark btn-sm', $Label = 'New Resource Category', $Icon = 'fa-plus') }}
    </div>
</div>
<div class="card-body pt-3 bg-light shadow-lg table-responsive">
    {!! Alert($icon = 'fa-info', $class = 'alert-danger', $Title = 'AfriChild Public Resources', $Msg = 'Manage Resources Categories') !!}
</div>

<div class="card-body pt-3 bg-light shadow-lg table-responsive">

    <table id="kt_datatable_example_2" class="mytable table table-striped table-row-bordered gy-5 gs-7">
        <thead>
            <tr class="fw-bolder fs-3 text-dark">
                <th>Category</th>
                <th>Brief Desc</th>
                <th class="bg-danger text-light fw-bolder">Thumbnail</th>
                <th>Update</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>

            @isset($Cat)
                @foreach ($Cat as $data)
                    <tr>

                        <td>{{ $data->CategoryTitle }}</td>
                        <td>{{ $data->BriefDesc }}</td>


                        <td>
                            <a data-fslightbox="lightbox-basic" href="{{ asset($data->Thumbnail) }}"
                                class="btn btn-sm btn-dark">
                                <i class="fas fa-binoculars" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            <a data-bs-toggle="modal" href="#Update{{ $data->id }}" class="btn btn-sm btn-dark">
                                <i class="fas fa-edit" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            {!! ConfirmBtn(
    $data = [
        'msg' => 'You want to delete this record',
        'route' => route('MassDelete', ['id' => $data->id, 'TableName' => 'resource_categories']),
        'label' => '<i class="fas fa-trash"></i>',
        'class' => 'btn btn-danger btn-sm deleteConfirm',
    ],
) !!}
                        </td>
                    </tr>
                @endforeach
            @endisset



        </tbody>

    </table>




</div>





@isset($Cat)
    @foreach ($Cat as $data)
        <form action="{{ route('AutoUpdate') }}" class="row" method="POST" enctype="multipart/form-data">
            @csrf


            <input type="hidden" name="UUID" value="{{ $data->UUID }}">
            {{ RunUpdateModal($ModalID = $data->id,$Extra =' <div class="mb-5"> <label for="" class="required form-label">Thumbnail</label> <input required type="file" class="form-control form-control-solid" name="Thumbnail" /> </div>',$csrf = '@csrf',$Title = 'Update the selected training event record',$RecordID = $data->id,$col = '3',$te = '12',$TableName = 'resource_categories') }}
        </form>
    @endforeach @endisset


    @include('Resources.NewCat')
