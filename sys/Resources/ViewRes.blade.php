@isset($Res)
    @foreach ($Res as $data)
        <div class="col-md-4 mt-5 mb-5">
            <div class="card card-rounded shadow-sm">
                <div class="card-header ">
                    <h4 class="card-title  text-dark fw-bolder">{{ $data->ResourceTitle }}
                    </h4>
                    <div class="card-toolbar">



                    </div>
                </div>
                <div class="card-body p-0">


                    <div class="text-center h-200px px-4"
                        style="background-image:url({{ asset($data->Thumbnail) }}); background-position: center; background-size: 100%">

                    </div>
                </div>
                <div class="card-footer" style="height: 70px; overflow-y:scroll">
                    <div class=" ">{{ $data->BriefDescription }}
                    </div>
                </div>
                <div class="card-footer bg-dark text-light">
                    <a data-doc="  {{ $data->ResourceTitle }} ({{ $data->BriefDescription }})"
                        data-source="{{ asset($data->AttachmentFile) }}"
                        data-bs-toggle="modal" href="#PdfJS"
                        class="btn mt-0   PdfViewer btn btn-danger shadow-lg text-light fw-bolder">
                        <i class="fas fa-file-pdf me-1" aria-hidden="true"></i> View
                    </a>
                    <a href="{{ asset($data->AttachmentFile) }}"
                        class="btn mt-0   btn btn-dark shadow-lg text-light fw-bolder">
                        <i class="fas fa-file-pdf me-1" aria-hidden="true"></i> Download File
                    </a>
                </div>

            </div>
        </div>
    @endforeach
@endisset


@include('Materials.pdf')
