<div class="row">
    <div class=" float-end">
        {{ HeaderBtn($Toggle = 'New', $Class = 'btn-dark btn-sm', $Label = 'New Resource ', $Icon = 'fa-plus') }}
    </div>
</div>
<div class="card-body pt-3 bg-light shadow-lg table-responsive">
    {!! Alert($icon = 'fa-info', $class = 'alert-danger', $Title = 'AfriChild Public Resources', $Msg = 'Manage Resources Categories') !!}
</div>

<div class="card-body pt-3 bg-light shadow-lg table-responsive">

    <table id="kt_datatable_example_2"
        class="mytable table table-striped table-row-bordered gy-5 gs-7">
        <thead>
            <tr class="fw-bolder fs-3 text-dark">
                <th>Theme</th>
                <th>Topic</th>
                <th>Sub-Topic</th>
                <th>Resource Title</th>
                <th>Resource Desc</th>
                <th class="bg-danger text-light fw-bolder">Thumbnail</th>
                <th class="bg-danger text-light fw-bolder">PDF File</th>
                <th>Update</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>

            @isset($Resources)
                @foreach ($Resources as $data)
                    <tr>

                        <td>{{ $data->Theme }}</td>
                        <td>{{ $data->Topic }}</td>
                        <td>{{ $data->SubTopic }}</td>
                        <td>{{ $data->ResourceTitle }}</td>
                        <td>{{ $data->BriefDescription }}</td>
                        <td>
                            <a data-fslightbox="lightbox-basic"
                                href="{{ asset($data->Thumbnail) }}"
                                class="btn btn-sm btn-dark">
                                <i class="fas fa-binoculars" aria-hidden="true"></i>
                            </a>
                        </td>

                        <td>
                            <a data-doc="  {{ $data->ResourceTitle }} ({{ $data->BriefDescription }})"
                                data-source="{{ asset($data->AttachmentFile) }}"
                                data-bs-toggle="modal" href="#PdfJS"
                                class="btn btn-sm  PdfViewer btn-danger"> <i
                                    class="fas fa-file-pdf" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            <a data-bs-toggle="modal" href="#Update{{ $data->id }}"
                                class="btn btn-sm btn-dark">
                                <i class="fas fa-edit" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            {!! ConfirmBtn(
    $data = [
        'msg' => 'You want to delete this record',
        'route' => route('MassDelete', ['id' => $data->id, 'TableName' => 'resources']),
        'label' => '<i class="fas fa-trash"></i>',
        'class' => 'btn btn-danger btn-sm deleteConfirm',
    ],
) !!}
                        </td>
                    </tr>
                @endforeach
            @endisset



        </tbody>

    </table>




</div>


@isset($Resources)
    @foreach ($Resources as $data)
        {{ UpdateModalHeader($Title = 'Update the selected  record', $ModalID = $data->id) }}
        <form novalidate action="{{ route('AutoUpdate') }}" class="" method="POST"
            enctype="multipart/form-data">
            @csrf

            <div class="row">

                <div class="col-md-12">
                    <div class="mt-3 mb-4">
                        <label for="" class="required form-label">Parent
                            Theme</label>

                        <select name="ThemeID" class="form-select" data-control="select2"
                            data-placeholder="Select an option">
                            <option value="{{ $data->ThemeID }}">
                                {{ $data->Theme }}</option>

                            @isset($Themes)
                                @foreach ($Themes as $dat)
                                    <option value="{{ $dat->ThemeID }}">
                                        {{ $dat->Title }}</option>
                                @endforeach
                            @endisset

                        </select>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="mt-3 mb-5">
                        <label for="" class="required form-label">Parent
                            Topic</label>

                        <select name="TopicID" class="form-select" data-control="select2"
                            data-placeholder="Select an option">
                            <option value="{{ $data->TopicID }}">{{ $data->Topic }}
                            </option>

                            @isset($Topics)
                                @foreach ($Topics as $dat1)
                                    <option value="{{ $dat1->TopicID }}">
                                        {{ $dat1->Title }}</option>
                                @endforeach
                            @endisset

                        </select>
                    </div>
                </div>


                <div class="col-md-6">
                    <div class="mt-3 mb-5">
                        <label for="" class="required form-label">Parent
                            SubTopic</label>

                        <select name="SubTopicID" class="form-select" data-control="select2"
                            data-placeholder="Select an option">
                            <option value="{{ $data->SubTopicID }}">
                                {{ $data->SubTopic }}
                            </option>

                            @isset($SubTopics)
                                @foreach ($SubTopics as $dat3)
                                    <option value="{{ $dat3->SubTopicID }}">
                                        {{ $dat3->Title }}</option>
                                @endforeach
                            @endisset

                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="mb-5 mt-4">
                        <label for="" class="required form-label">Thumbnail</label>
                        <input required type="file" class="form-control form-control-solid"
                            name="Thumbnail" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-5 mt-4">
                        <label for="" class="required form-label">Resource
                            File (PDF Only)</label>
                        <input required type="file" class="form-control form-control-solid"
                            name="AttachmentFile" />
                    </div>
                </div>



                <input type="hidden" name="id" value="{{ $data->id }}">

                <input type="hidden" name="TableName" value="resources">

                {{ RunUpdateModalFinal($ModalID = $data->id, $Extra = '', $csrf = null, $Title = null, $RecordID = $data->id, $col = '6', $te = '12', $TableName = 'resources') }}
            </div>


            {{ UpdateModalFooter() }}

        </form>
    @endforeach
@endisset


@include('Resources.NewResources')
@include('PDF.PDFJS')
