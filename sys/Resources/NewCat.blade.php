<div class="modal bg-white fade" id="New">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content shadow-none">
            <div class="modal-header">
                <h5 class="modal-title">Create new resource category</h5>
                <form enctype="multipart/form-data" action="{{ route('AutoInsert') }}" method="POST">
                    <div class="row">
                        <div class="col-12 float-end">
                            <button type="button" class="btn btn-dark  float-end" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn  btn-danger mx-2 float-end">Save changes</button>

                        </div>
                    </div>
            </div>


            @csrf
            {{-- modal body --}}
            {{-- modal body --}}
            {{-- modal body --}}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mb-5">
                            <label for="" class="required form-label">Thumbnail</label>
                            <input required type="file" class="form-control form-control-solid" name="Thumbnail" />
                        </div>
                    </div>

                    @foreach ($Form as $data)
                        @if ($data['type'] == 'string')
                            {{ CreateInputText($data, $placeholder = null, $col = '6') }}
                        @elseif ($data['type'] == 'integer')
                            {{ CreateInputInteger($data, $placeholder = null, $col = '3') }}
                        @elseif ($data['type'] == 'date' || $data['type'] == 'datetime')
                            {{ CreateInputDate($data, $placeholder = null, $col = '3') }}
                        @endif
                    @endforeach

                </div>

                <input required type="hidden" name="TableName" value="resource_categories">

                <input required required type="hidden" name="created_at" value="{{ date('Y-m-d H:i:s') }}">



                <input required type="hidden" name="CID"
                    value="{{ \Hash::make(uniqid() . 'AFC' . date('Y-m-d H:I:S')) }}">

                <input required type="hidden" name="UUID"
                    value="{{ \Hash::make(uniqid() . 'AFC' . date('Y-m-d H:I:S')) }}">



                <div class="row">
                    @foreach ($Form as $data)
                        @if ($data['type'] == 'text')
                            {{ CreateInputEditor($data, $placeholder = null, $col = '12') }}
                        @endif
                    @endforeach
                    </form>
                </div>
            </div>
            {{-- modal body --}}
            {{-- modal body --}}
            {{-- modal body --}}


        </div>
    </div>
</div>
