@isset($Cats)
    @foreach ($Cats as $data)
        <div class="col-md-4 mt-5 mb-5">
            <div class="card shadow-sm">
                <div class="card-header bg-danger text-light ">
                    <h4 class="card-title text-light fw-bolder">{{ $data->CategoryTitle }}</h4>
                    <div class="card-toolbar">

                    </div>
                </div>
                <div class="card-body p-0">


                    <div class="text-center h-200px px-4"
                        style="background-image:url({{ asset($data->Thumbnail) }}); background-position: center; background-size: 100%">

                    </div>

                    <div class="card-footer" style="height: 70px; overflow-y:scroll">
                        <div class=" ">{{ $data->BriefDesc }}
                        </div>
                    </div>
                    <div class="card-footer bg-light text-light">
                        <a href="{{ route('ViewCategories', ['id' => $data->id]) }}" class="btn  btn-danger shadow-lg"> <i
                                class="fas fa-file me-1" aria-hidden="true"></i> View
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endisset
