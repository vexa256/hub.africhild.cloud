<div class="modal fade" id="Desc{{ $info->id }}">
    <div class="modal-dialog modal-dialog-scrollable modal-fullscreen ">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    {{ $Title }}
                </h5>

                <!--begin::Close-->
                <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal"
                    aria-label="Close">
                    <span class="svg-icon svg-icon-2x">
                        <i class="fas fa-3x text-dark fa-times" aria-hidden="true"></i>
                    </span>
                </div>
                <!--end::Close-->
            </div>

            <div class="modal-body">


                <div class="mb-10 col-md-12">
                    <label for="exampleFormControlInput1" class="required form-label">Description/Details</label>
                    <textarea name="Desc">
                        {!! $info->{$col} !!}
                        </textarea>
                </div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-dark shadow-lg" data-bs-dismiss="modal">Close</button>


            </div>

        </div>
    </div>
</div>
