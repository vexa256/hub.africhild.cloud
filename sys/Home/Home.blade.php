<div class="card">
    <!--begin::Card body-->
    <div class="card-body p-0">
        <!--begin::Wrapper-->
        <div class="card-px text-center py-20 my-10">
            <!--begin::Title-->
            <h2 class="fs-2x fw-bolder mb-10 text-danger">Hello @auth
                    {{ Auth::user()->name }}
                @endauth
            </h2>
            <!--end::Title-->
            <!--begin::Description-->
            <p class="text-dark fs-4 fw-bold mb-10">Welcome to the AfriChild Knowledge Hub.
                Explore diverse public
                resources

            </p>
            <!--end::Description-->
            <!--begin::Action-->
            <a href="{{ route('ViewThemes') }}"
                class="btn btn-danger shadow-lg mx-2">Digital Library</a>
            {{-- <a href="{{ route('ViewTrainings') }}"
                class="btn btn-dark shadow-lg mx-2">Trainings</a> --}}

            <!--end::Action-->
        </div>
        <!--end::Wrapper-->
        <!--begin::Illustration-->

        <!--end::Illustration-->
    </div>
    <!--end::Card body-->
</div>
