<div class="modal bg-white fade" id="New">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content shadow-none">
            <div class="modal-header">
                <h5 class="modal-title">Create a new admin account

                    <span class="text-danger font-weight-bolder">

                    </span>
                </h5>
                <form enctype="multipart/form-data" action="{{ route('AutoInsert') }}" method="POST">
                    <div class="row">
                        <div class="col-12 float-end">
                            <button type="button" class="btn btn-dark  float-end" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn  btn-danger mx-2 float-end">Save changes</button>

                        </div>
                    </div>
            </div>


            @csrf
            {{-- modal body --}}
            {{-- modal body --}}
            {{-- modal body --}}
            <div class="modal-body">
                <div class="row">
                    <div class="mb-5">
                        <label for="" class="required form-label">Select role</label>

                        <select name="Role" class="form-select" data-control="select2"
                            data-placeholder="Select an option">
                            <option value="SuperAdmin">Super Admin (All Rights)</option>
                            <option value="Admin"> Admin No delete rights)</option>


                        </select>
                    </div>

                    @foreach ($Form as $data)
                        @if ($data['type'] == 'string')
                            {{ CreateInputText($data, $placeholder = null, $col = '4') }}
                        @elseif ($data['type'] == 'integer')
                            {{ CreateInputInteger($data, $placeholder = null, $col = '3') }}
                        @elseif ($data['type'] == 'date' || $data['type'] == 'datetime')
                            {{ CreateInputDate($data, $placeholder = null, $col = '3') }}
                        @endif
                    @endforeach

                </div>

                <input required type="hidden" name="TableName" value="users">

                <input required required type="hidden" name="created_at" value="{{ date('Y-m-d H:i:s') }}">





                <input required type="hidden" name="UID"
                    value="{{ \Hash::make(uniqid() . 'AFC' . date('Y-m-d H:I:S')) }}">


                <div class="row">
                    @foreach ($Form as $data)
                        @if ($data['type'] == 'text')
                            {{ CreateInputEditor($data, $placeholder = null, $col = '12') }}
                        @endif
                    @endforeach
                    </form>
                </div>
            </div>
            {{-- modal body --}}
            {{-- modal body --}}
            {{-- modal body --}}


        </div>
    </div>
</div>
