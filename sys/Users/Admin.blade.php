<div class="row">
    <div class=" float-end">
        {{ HeaderBtn($Toggle = 'New', $Class = 'btn-dark btn-sm', $Label = 'New Admin', $Icon = 'fa-plus') }}
    </div>
</div>

<div class="card-body pt-3 bg-light shadow-lg table-responsive">
    {!! Alert($icon = 'fa-info', $class = 'alert-primary', $Title = 'Advanced Settings', $Msg = 'Manage all authorized Admin and Super User accounts') !!}
</div>
<div class="card-body pt-3 bg-light shadow-lg table-responsive">

    <table class="mytable table table-striped table-row-bordered gy-5 gs-7">
        <thead>
            <tr class="fw-bolder fs-3 text-dark">
                <th>Account Holder</th>
                <th>Email/Usernamer</th>
                <th class="bg-danger text-light fw-bolder">Account role</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>

            @isset($Admin)
                @foreach ($Admin as $data)
                    <tr>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->Role }}</td>


                        <td>
                            {!! ConfirmBtn(
    $data = [
        'msg' => 'You want to delete this admin account.',
        'route' => route('MassDelete', ['id' => $data->id, 'TableName' => 'users']),
        'label' => '<i class="fas fa-trash"></i>',
        'class' => 'btn btn-danger btn-sm deleteConfirm',
    ],
) !!}
                        </td>
                    </tr>
                @endforeach
            @endisset



        </tbody>

    </table>

</div>
@include('Users.NewAdmin')
