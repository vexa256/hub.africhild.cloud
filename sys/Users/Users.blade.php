<div class="card-body pt-3 bg-light shadow-lg table-responsive">
    {!! Alert($icon = 'fa-info', $class = 'alert-primary', $Title = 'Advanced Settings', $Msg = 'Manage all authorized Admin and Super User accounts') !!}
</div>
<div class="card-body pt-3 bg-light shadow-lg table-responsive">

    <table class="mytable table table-striped table-row-bordered gy-5 gs-7">
        <thead>
            <tr class="fw-bolder fs-3 text-dark">
                <th>Account Holder</th>
                <th>Email/Usernamer</th>
                <th>Proffession</th>
                <th>Phone</th>
                <th>Sex</th>
                <th>Nationality</th>

                <th class="bg-danger text-light fw-bolder">Account role</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>

            @isset($Admin)
                @foreach ($Admin as $data)
                    <tr>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->Proffession }}</td>
                        <td>{{ $data->Phone }}</td>
                        <td>{{ $data->Sex }}</td>
                        <td>{{ $data->Nationality }}</td>
                        <td>{{ $data->Role }}</td>


                        <td>
                            {!! ConfirmBtn(
    $data = [
        'msg' => 'You want to delete this users account.',
        'route' => route('MassDelete', ['id' => $data->id, 'TableName' => 'users']),
        'label' => '<i class="fas fa-trash"></i>',
        'class' => 'btn btn-danger btn-sm deleteConfirm',
    ],
) !!}
                        </td>
                    </tr>
                @endforeach
            @endisset



        </tbody>

    </table>

</div>
